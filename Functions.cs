using HarmonyLib;
using System.Collections.Generic;
using UnityEngine;

namespace LISBTSAspectRatio {
	public static class Shared {
		static public List<int> refreshRates = new List<int>();

		static public void CheckRefreshRates() {
			if (refreshRates.Count == 0) {
				Resolution[] resolutions = Screen.resolutions;

				refreshRates.Add(-1);
				refreshRates.Add(60);
				refreshRates.Add(30);

				foreach (Resolution res in resolutions) {
					if (!refreshRates.Contains(res.refreshRate)) {
						refreshRates.Add(res.refreshRate);
					}
				}
			}
		}
	}

	[HarmonyPatch(typeof(T_1005C221.T_A00AC79F), nameof(T_1005C221.T_A00AC79F.SetVSyncNow))]
	public static class T_A00AC79F_SetVSyncNow {
		static void Postfix(int _110258F68) {
			Shared.CheckRefreshRates();

			QualitySettings.vSyncCount = 0;

			Application.targetFrameRate = Shared.refreshRates[_110258F68];
		}
	}

	[HarmonyPatch(typeof(T_1005C221.T_A00AC79F), nameof(T_1005C221.T_A00AC79F.GetActiveVSyncCount))]
	public static class T_A00AC79F_GetActiveVSyncCount {
		static bool Prefix(ref int __result) {
			Shared.CheckRefreshRates();

			__result = Shared.refreshRates.IndexOf(Application.targetFrameRate);

			return false;
		}
	}

	[HarmonyPatch(typeof(T_39A16206), "Setup")]
	public static class T_39A16206_Setup {
		static void Prefix(T_39A16206 __instance) {
			__instance.m_VSyncUI.m_optionText = Shared.refreshRates.ConvertAll<string>(
				rate => rate == -1 ? "No" : $"{rate} FPS"
			).ToArray();

			__instance.m_VSyncUI.m_optionTextKeys = Shared.refreshRates.ConvertAll<string>(
				rate => rate == -1 ? "GLOBAL_CODE_NO" : $"SETTINGS_UI_{rate}FPS"
			).ToArray();
		}
	}

	[HarmonyPatch(typeof(T_39A16206.T_59BAA7AA), MethodType.Constructor)]
	public static class T_59BAA7AA_Constructor {
		static void Postfix(T_39A16206.T_59BAA7AA __instance) {
			Resolution res = T_1005C221.T_A00AC79F.GetCurrentScreenResolution();

			T_DE3910C3.s_instance.targetAspectRatioWidth = ((float) res.width) / ((float) res.height);
			T_DE3910C3.s_instance.targetAspectRatioHeight = 1f;

			T_DE3910C3.SetAspectRatioMode((eAspectRatioMode) (-1));

			T_1005C221.T_A00AC79F.SetScreenResolution(res, _127E98ACE: false);
			T_D90B5C2A.onScreenResize();
		}
	}

	[HarmonyPatch(typeof(T_39A16206.T_59BAA7AA), nameof(T_39A16206.T_59BAA7AA.SetScreenResolution))]
	public static class T_59BAA7AA_SetScreenResolution {
		static void Postfix(T_39A16206.T_59BAA7AA __instance) {
			Resolution res = __instance.m_currentResolution;

			T_DE3910C3.s_instance.targetAspectRatioWidth = ((float) res.width) / ((float) res.height);
			T_DE3910C3.s_instance.targetAspectRatioHeight = 1f;

			T_DE3910C3.SetAspectRatioMode((eAspectRatioMode) (-1));

			T_1005C221.T_A00AC79F.SetScreenResolution(res, _127E98ACE: false);
			T_D90B5C2A.onScreenResize();
		}
	}

	[HarmonyPatch(typeof(T_39A16206.T_59BAA7AA), nameof(T_39A16206.T_59BAA7AA.GenerateResolutions))]
	public static class Functions {
		static void Postfix(T_39A16206.T_59BAA7AA __instance) {
			__instance.m_resolutions.Clear();
			__instance.m_resolutions.AddRange(Screen.resolutions);

			Resolution res = new Resolution();
			res.width = Display.main.systemWidth;
			res.height = Display.main.systemHeight;
			__instance.m_resolutions.Add(res);

			for (int num = __instance.m_resolutions.Count - 1; num >= 0; num--) {
				for (int num2 = num - 1; num2 >= 0; num2--) {
					if (__instance.m_resolutions[num].height == __instance.m_resolutions[num2].height && __instance.m_resolutions[num].width == __instance.m_resolutions[num2].width) {
						__instance.m_resolutions.RemoveAt(num);
						break;
					}
				}
			}

			__instance.m_resolutionsAsString.Clear();
			for (int i = 0; i < __instance.m_resolutions.Count; i++) {
				__instance.m_resolutionsAsString.Add(__instance.m_resolutions[i].width + "X" + __instance.m_resolutions[i].height);
			}
		}
	}

	[HarmonyPatch(typeof(T_6B664603), nameof(T_6B664603.SetMode))]
	public static class T_6B664603_SetMode {
		static void Prefix(T_6B664603 __instance, eGameMode _1C57B7248) {
			Resolution res = T_1005C221.T_A00AC79F.GetCurrentScreenResolution();

			if (_1C57B7248 == eGameMode.kVideo) {
				T_DE3910C3.SetAspectRatioMode(eAspectRatioMode.kSixteenByNine);
				T_DE3910C3.s_instance.targetAspectRatioWidth = 16f;
				T_DE3910C3.s_instance.targetAspectRatioHeight = 9f;
				T_D90B5C2A.onScreenResize();
				return;
			}

			T_DE3910C3.s_instance.targetAspectRatioWidth = ((float) res.width) / ((float) res.height);
			T_DE3910C3.s_instance.targetAspectRatioHeight = 1f;
			T_DE3910C3.SetAspectRatioMode((eAspectRatioMode) (-1));
			T_D90B5C2A.onScreenResize();
		}
	}
}