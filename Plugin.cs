﻿using BepInEx;
using BepInEx.Logging;
using HarmonyLib;

namespace LISBTSAspectRatio {
    [BepInPlugin("uk.co.connieprice.lisbtsaspectratio", "Life is Strange: BTS - Aspect Ratio", "1.1.0")]
    public class LISBTSAspectRatioPlugin : BaseUnityPlugin {
        public static ManualLogSource BIPLogger;

        private void Awake() {
            BIPLogger = Logger;
            BIPLogger.LogInfo($"Plugin uk.co.connieprice.lisbtsaspectratio loaded!");
            new Harmony("uk.co.connieprice.lisbtsaspectratio").PatchAll();
        }
    }
}
